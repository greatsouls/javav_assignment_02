<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
  <head>
    <title>Team Management</title>
    <link rel="stylesheet" 
          type="text/css" 
          href="<c:url value="/resources/style.css" />" >
  </head>
  <body>
    <h1>Welcome to Team Management</h1>

    <a href="<c:url value="/player/create" />">Create player</a> |
    <a href="<c:url value="/trainer/create" />">Create trainer</a>
  </body>
</html>
