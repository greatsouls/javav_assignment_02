<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page session="false" %>
<html>
<head>
    <title>Trainer Profile</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/style.css" />">
</head>
<body>
<h1>Trainer Profile</h1>
<table border="0" cellspacing="2">
    <tr>
        <td><spring:message code="first.name"/></td>
        <td><c:out value="${trainer.firstName}"/><br/></td>
    </tr>
    <tr>
        <td><spring:message code="last.name"/></td>
        <td><c:out value="${trainer.lastName}"/><br/></td>
    </tr>
    <tr>
        <td><spring:message code="age"/></td>
        <td><c:out value="${trainer.age}"/><br/></td>
    </tr>
    <tr>
        <td><spring:message code="annualSalary"/></td>
        <td><c:out value="${trainer.annualSalary}"/><br/></td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <button type="button" onclick="document.location='/assignment_02/'"><spring:message code="Return"/></button>
        </td>
    </tr>
</table>
</body>
</html>