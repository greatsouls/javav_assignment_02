package com.skynetgate.web;

import com.skynetgate.beans.*;
import com.skynetgate.factories.FactoryTrainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.Currency;
import java.util.Locale;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by Emil Gochev.
 */

@Controller
@RequestMapping("/trainer")
public class TrainerController {

    private TrainerContainer trainerContainer;
    private Trainer trainer;

    private Currency currency = Currency.getInstance(Locale.US);

    @Autowired
    private FactoryTrainer factoryTrainer;

    @Autowired
    public TrainerController(TrainerContainer trainerContainer) {
        this.trainerContainer = trainerContainer;
    }

    @RequestMapping(value = "/create", method = GET)
    public String showRegistrationForm(Model model) {
        model.addAttribute(new TrainerContainer());
        return "createTrainerForm";
    }

    @RequestMapping(value = "/create", method = POST)
    public String processCreateTrainer(
            @Valid TrainerContainer trainerContainer,
            Errors errors) {
        if (errors.hasErrors()) {
            return "createTrainerForm";
        }

        trainer = factoryTrainer.createTrainer(trainerContainer.getFirstName(), trainerContainer.getLastName(),
                trainerContainer.getAge(), trainerContainer.getAnnualSalary(), currency);

        if (trainer == null) {
            return "createTrainerForm";
        } else {
            return "redirect:/trainer/" + trainer.getFirstName();
        }
    }

    @RequestMapping(value = "/{firstName}", method = GET)
    public String showCreatedTrainer(@PathVariable String firstName, Model model) {
        model.addAttribute(trainer);
        return "profileTrainer";
    }
}
