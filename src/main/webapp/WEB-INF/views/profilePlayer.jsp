<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page session="false" %>
<html>
<head>
    <title>Player Profile</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/style.css" />">
</head>
<body>
<h1>Player Profile</h1>
<table border="0" cellspacing="2">
    <tr>
        <td><spring:message code="first.name"/></td>
        <td><c:out value="
                                ${playerDefender.firstName}
                                ${playerForward.firstName}
                                ${playerGoalkeeper.firstName}
                                ${playerMidfielder.firstName}
                                "/><br/>
        </td>
    </tr>
    <tr>
        <td><spring:message code="last.name"/></td>
        <td><c:out value="
                                ${playerDefender.lastName}
                                ${playerForward.lastName}
                                ${playerGoalkeeper.lastName}
                                ${playerMidfielder.lastName}
                                "/><br/>
        </td>
    </tr>
    <tr>
        <td><spring:message code="age"/></td>
        <td><c:out value="
                                ${playerDefender.age}
                                ${playerForward.age}
                                ${playerGoalkeeper.age}
                                ${playerMidfielder.age}
                                "/><br/>
        </td>
    </tr>
    <tr>
        <td><spring:message code="countryOfBirth"/></td>
        <td><c:out value="
                                ${playerDefender.countryOfBirth}
                                ${playerForward.countryOfBirth}
                                ${playerGoalkeeper.countryOfBirth}
                                ${playerMidfielder.countryOfBirth}
                                "/><br/>
        </td>
    </tr>
    <tr>
        <td><spring:message code="position"/></td>
        <td><c:out value="
                                ${playerDefender.position}
                                ${playerForward.position}
                                ${playerGoalkeeper.position}
                                ${playerMidfielder.position}
                                "/><br/>
        </td>
    </tr>
    <tr>
        <td><spring:message code="annualSalary"/></td>
        <td><c:out value="
                                ${playerDefender.annualSalary}
                                ${playerForward.annualSalary}
                                ${playerGoalkeeper.annualSalary}
                                ${playerMidfielder.annualSalary}
                                "/><br/>
        </td>
    </tr>
    <tr>
        <td><spring:message code="numberOfGoals"/></td>
        <td><c:out value="
                                ${playerDefender.statistics.numberOfGoals}
                                ${playerForward.statistics.numberOfGoals}
                                ${playerGoalkeeper.statistics.numberOfGoals}
                                ${playerMidfielder.statistics.numberOfGoals}
                                "/><br/>
        </td>
    </tr>
    <tr>
        <td><spring:message code="numberOfBookings"/></td>
        <td><c:out value="
                                ${playerDefender.statistics.numberOfBookings}
                                ${playerForward.statistics.numberOfBookings}
                                ${playerGoalkeeper.statistics.numberOfBookings}
                                ${playerMidfielder.statistics.numberOfBookings}
                                "/><br/>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <button type="button" onclick="document.location='/assignment_02/'"><spring:message code="Return"/></button>
        </td>
    </tr>
</table>
</body>
</html>